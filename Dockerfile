FROM golang:1.13.0-buster

WORKDIR /go/src/gitlab.com/ace-api
COPY . .

RUN go get github.com/sirupsen/logrus 
RUN go get go.mongodb.org/mongo-driver/bson
RUN go get github.com/gorilla/mux
RUN go get golang.org/x/crypto/bcrypt
RUN export GOBIN=/go/bin;go install main.go

EXPOSE 8000

CMD ["main"]

