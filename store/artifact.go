package store

import (
	"context"

	log "github.com/sirupsen/logrus"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"

	"go.mongodb.org/mongo-driver/mongo"
)

type Artifact struct {
	DojoID   primitive.ObjectID `bson:"dojoId"`
	FileName string             `bson:"name"`
	FileType string             `bson:"type"`
	Mime     []byte             `bson:"data"`
}

type ArtifactSummary struct {
	ID       string `bson:"_id" json:"id"`
	FileName string `bson:"name" json:"name"`
	FileType string `bson:"type" json:"type"`
}

func SaveArtifact(dojoID string, fileName string, fileType string, fileByte []byte, db *mongo.Database) error {
	if did, err := primitive.ObjectIDFromHex(dojoID); err != nil {
		return err
	} else {
		filter := bson.M{"_id": dojoID + fileName}
		upsert := true
		options := options.ReplaceOptions{Upsert: &upsert}
		_, err := db.Collection("artifacts").ReplaceOne(context.Background(), filter, Artifact{
			did, fileName, fileType, fileByte}, &options)

		return err
	}
}

func GetArtifacts(dojoID string, db *mongo.Database) ([]ArtifactSummary, error) {
	if did, err := primitive.ObjectIDFromHex(dojoID); err != nil {
		return nil, err
	} else {
		filter := bson.M{"dojoId": did}
		if cursor, err := db.Collection("artifacts").Find(context.Background(), filter); err != nil {
			log.Error(err)
			return nil, err
		} else {
			defer cursor.Close(context.Background())

			var artifacts []ArtifactSummary
			if err := cursor.All(context.Background(), &artifacts); err != nil {
				return nil, err
			}

			if artifacts == nil {
				return []ArtifactSummary{}, nil
			}
			return artifacts, nil
		}
	}
}

func GetArtifact(dojoID string, artifactID string, db *mongo.Database) (*Artifact, error) {
	filter := bson.M{"_id": dojoID + artifactID}
	var artifact Artifact
	if err := db.Collection("artifacts").FindOne(context.Background(), filter).Decode(&artifact); err != nil {
		return nil, err
	} else {
		return &artifact, nil
	}
}
