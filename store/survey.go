package store

import (
	"context"

	"gitlab.com/ace-api/models"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

// GetSurvey = retrieve survey doc from store
func GetSurvey(surveyID string, db *mongo.Database) (*models.Survey, error) {
	if oid, err := primitive.ObjectIDFromHex(surveyID); err == nil {
		var survey models.Survey
		filter := bson.M{"_id": oid}
		if err := db.Collection("surveys").FindOne(context.Background(), filter).Decode(&survey); err != nil {
			return nil, err
		}
		return &survey, nil
	} else {
		return nil, err
	}
}
