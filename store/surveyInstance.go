package store

import (
	"context"
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.com/ace-api/models"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

// CreateSurveyInstanceModel = survey instance document model
type CreateSurveyInstanceModel struct {
	SurveyID    primitive.ObjectID `bson:"surveyID,omitempty" json:"surveyId"`
	Type        string             `bson:"type" json:"type"`
	Version     string             `bson:"version" json:"version"`
	Title       string             `bson:"title" json:"title"`
	Subtitle    string             `bson:"subtitle" json:"subtitle"`
	Date        *time.Time         `bson:"date" json:"date"`
	Questions   []models.Question  `bson:"questions" json:"questions"`
	Description string             `bson:"description,omitempty" json:"description"`
	Group       string             `bson:"group" json:"group"`
	Team        string             `bson:"team" json:"team"`
	Quarter     string             `bson:"quarter" json:"quarter"`
	From        string             `bson:"from" json:"from"`
	To          string             `bson:"to" json:"to"`
	Answers     [][]int            `bson:"answers" json:"answers"`
}

type SurveyInstanceModel struct {
	ID          primitive.ObjectID `bson:"_id,omitempty" json:"id"`
	SurveyID    primitive.ObjectID `bson:"surveyID,omitempty" json:"surveyId"`
	Type        string             `bson:"type" json:"type"`
	Version     string             `bson:"version" json:"version"`
	Title       string             `bson:"title" json:"title"`
	Subtitle    string             `bson:"subtitle" json:"subtitle"`
	Date        *time.Time         `bson:"date" json:"date"`
	Questions   []models.Question  `bson:"questions" json:"questions"`
	Description string             `bson:"description,omitempty" json:"description"`
	Group       string             `bson:"group" json:"group"`
	Team        string             `bson:"team" json:"team"`
	Quarter     string             `bson:"quarter" json:"quarter"`
	From        string             `bson:"from" json:"from"`
	To          string             `bson:"to" json:"to"`
	Answers     [][]int            `bson:"answers" json:"answers"`
}

type SurveyInstanceAnswerModel struct {
	Answers []int `bson:"answers"`
}

type SurveyFeedbackModel struct {
	SurveyID   primitive.ObjectID `bson:"surveyId"`
	InstanceID primitive.ObjectID `bson:"instanceId"`
	Rating     int                `bson:"rating"`
	Feedback   string             `bson:"feedback"`
	Time       *time.Time         `bson:"timestamp"`
}

type SurveyInstanceGroup struct {
	ID *string `bson:"_id" json:"id"`
}

// CreateSurveyInstanceDoc to create a survey instance
func CreateSurveyInstanceDoc(newSurveyInstance CreateSurveyInstanceModel, db *mongo.Database) (string, error) {
	if res, err := db.Collection("surveys_instances").InsertOne(context.Background(), newSurveyInstance); err != nil {
		return "", err
	} else {
		return res.InsertedID.(primitive.ObjectID).Hex(), nil
	}
}

// GetSurveyInstances gets a list of survey instances
func GetSurveyInstances(surveyID string, group string, team string, db *mongo.Database) ([]SurveyInstanceModel, error) {
	if oid, err := primitive.ObjectIDFromHex(surveyID); err != nil {
		return nil, err
	} else {
		filter := bson.M{"surveyID": oid}
		if len(group) > 0 {
			filter["group"] = group
		}
		if len(team) > 0 {
			filter["team"] = team
		}
		if cursor, err := db.Collection("surveys_instances").Find(context.Background(), filter); err != nil {
			log.Error(err)
			return nil, err
		} else {
			defer cursor.Close(context.Background())

			var surveyInstances []SurveyInstanceModel
			if err := cursor.All(context.Background(), &surveyInstances); err != nil {
				return nil, err
			}

			return surveyInstances, nil
		}
	}
}

func GetSurveyInstance(instanceID string, db *mongo.Database) (*SurveyInstanceModel, error) {
	if oid, err := primitive.ObjectIDFromHex(instanceID); err != nil {
		return nil, err
	} else {
		filter := bson.M{"_id": oid}
		var survey SurveyInstanceModel
		if err := db.Collection("surveys_instances").FindOne(context.Background(), filter).Decode(&survey); err != nil {
			return nil, err
		} else {
			return &survey, nil
		}
	}
}

func UpdateSurveyResponse(instanceID string, answers []int, db *mongo.Database) error {
	if oid, err := primitive.ObjectIDFromHex(instanceID); err != nil {
		return err
	} else {
		filter := bson.M{"_id": oid}
		insertAnswers := bson.M{"$push": SurveyInstanceAnswerModel{answers}}
		if _, err := db.Collection("surveys_instances").UpdateOne(context.Background(), filter, insertAnswers); err != nil {
			return err
		}
	}
	return nil
}

func DeleteSurveyInstance(surveyID string, instanceID string, db *mongo.Database) error {
	if osID, err := primitive.ObjectIDFromHex(surveyID); err != nil {
		return err
	} else {
		if oiID, err := primitive.ObjectIDFromHex(instanceID); err != nil {
			return err
		} else {
			filter := bson.M{"_id": oiID, "surveyID": osID}
			if _, err := db.Collection("surveys_instances").DeleteOne(context.Background(), filter); err != nil {
				return err
			}
		}
	}
	return nil
}

func AddSurveyFeedback(doc SurveyFeedbackModel, db *mongo.Database) error {
	if _, err := db.Collection("survey_feedbacks").InsertOne(context.Background(), doc); err != nil {
		return err
	}
	return nil
}

func GetSurveyInstanceGroups(surveyID string, db *mongo.Database) ([]SurveyInstanceGroup, error) {
	if sid, err := primitive.ObjectIDFromHex(surveyID); err != nil {
		return nil, err
	} else {
		pipeline := []bson.M{
			bson.M{"$match": bson.M{"surveyID": sid}},
			bson.M{"$group": bson.M{"_id": "$group"}}}
		if cursor, err := db.Collection("surveys_instances").Aggregate(context.Background(), pipeline); err != nil {
			return nil, err
		} else {
			defer cursor.Close(context.Background())

			var groupNames []SurveyInstanceGroup
			if err := cursor.All(context.Background(), &groupNames); err != nil {
				return nil, err
			}

			return groupNames, nil
		}
	}
}

func GetSurveyInstanceTeams(surveyID string, db *mongo.Database) ([]SurveyInstanceGroup, error) {
	if sid, err := primitive.ObjectIDFromHex(surveyID); err != nil {
		return nil, err
	} else {
		pipeline := []bson.M{
			bson.M{"$match": bson.M{"surveyID": sid}},
			bson.M{"$group": bson.M{"_id": "$team"}}}
		if cursor, err := db.Collection("surveys_instances").Aggregate(context.Background(), pipeline); err != nil {
			return nil, err
		} else {
			defer cursor.Close(context.Background())

			var teams []SurveyInstanceGroup
			if err := cursor.All(context.Background(), &teams); err != nil {
				return nil, err
			}

			return teams, nil
		}
	}
}
