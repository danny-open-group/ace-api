package main

import (
	"fmt"
	"net/http"
	"os"
	"time"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	"gitlab.com/ace-api/handlers"
	"gitlab.com/ace-api/middleware"
	"gitlab.com/ace-api/models"
)

func main() {
	mongo := "mongo42"
	if os.Getenv("env") == "dev" {
		mongo = "localhost"
	}
	services, database, err := models.NewServices("mongodb://"+mongo+":27017", "ACE")
	if err != nil {
		panic(err)
	}
	defer services.Close()

	usersH := handlers.NewUsers(services.User, services.Session)
	teamsH := handlers.NewTeams(services.Team)
	dojoH := handlers.NewDojos(services.Dojo, services)
	dojoValuesH := handlers.NewDojoValues(services.DojoValues)
	settingH := handlers.NewSettings(services.Setting)
	personH := handlers.NewPersons(services.Person)
	surveyH := handlers.NewSurveys(services.Survey)

	requireUserMw := middleware.RequireUser{
		SessionService: services.Session,
	}

	fmt.Printf("Starting mux...\n")
	r := mux.NewRouter()

	r.HandleFunc("/login", usersH.Login).Methods("POST")

	// User
	createUser := requireUserMw.ApplyFn(usersH.Create)

	r.HandleFunc("/register", createUser).Methods("POST")

	// Team
	createTeam := requireUserMw.ApplyFn(teamsH.Create)
	listTeams := requireUserMw.ApplyFn(teamsH.List)
	retrieveTeams := requireUserMw.ApplyFn(teamsH.Retrieve)
	updateTeams := requireUserMw.ApplyFn(teamsH.Update)

	r.HandleFunc("/team", createTeam).Methods("POST")
	r.HandleFunc("/team", listTeams).Methods("GET")
	r.HandleFunc("/team/{id}", retrieveTeams).Methods("GET")
	r.HandleFunc("/team/{id}", updateTeams).Methods("PUT")
	r.HandleFunc("/team/{id}/dojo/{dojoI}", teamsH.AddRating).Methods("POST")

	// Dojo
	createDojo := requireUserMw.ApplyFn(dojoH.Create)
	updateDojo := requireUserMw.ApplyFn(dojoH.Update)
	listDojos := requireUserMw.ApplyFn(dojoH.List)
	retrieveDojos := requireUserMw.ApplyFn(dojoH.Retrieve)

	r.HandleFunc("/dojo", createDojo).Methods("POST")
	r.HandleFunc("/dojo", listDojos).Methods("GET")
	r.HandleFunc("/dojo/{id}", retrieveDojos).Methods("GET")
	r.HandleFunc("/dojo/{id}", updateDojo).Methods("PUT")
	r.HandleFunc("/dojo/{id}/rating", dojoH.AddRating).Methods("POST")

	//DojoValues
	listDojoValues := requireUserMw.ApplyFn(dojoValuesH.ListDojoValues)

	r.HandleFunc("/dojovalues", dojoValuesH.SetDojoValue).Methods("POST")
	r.HandleFunc("/dojovalues/{dojoID}", listDojoValues).Methods("GET")

	//Settings
	setSetting := requireUserMw.ApplyFn(settingH.SetSetting)
	listSettings := requireUserMw.ApplyFn(settingH.ListSettings)
	deleteSetting := requireUserMw.ApplyFn(settingH.DeleteSetting)
	getSetting := requireUserMw.ApplyFn(settingH.GetSetting)

	r.HandleFunc("/setting", setSetting).Methods("POST")
	r.HandleFunc("/setting", listSettings).Methods("GET")
	r.HandleFunc("/setting/{key}", getSetting).Methods("GET")
	r.HandleFunc("/setting/{key}", deleteSetting).Methods("DELETE")

	// Person
	createPerson := requireUserMw.ApplyFn(personH.Create)
	listPersons := requireUserMw.ApplyFn(personH.List)
	retrievePerson := requireUserMw.ApplyFn(personH.Retrieve)
	updatePerson := requireUserMw.ApplyFn(personH.Update)

	r.HandleFunc("/person", createPerson).Methods("POST")
	r.HandleFunc("/person", listPersons).Methods("GET")
	r.HandleFunc("/person/{id}", retrievePerson).Methods("GET")
	r.HandleFunc("/person/{id}", updatePerson).Methods("PUT")

	// Surveys
	createSurvey := requireUserMw.ApplyFn(surveyH.Create)
	listSurveys := requireUserMw.ApplyFn(surveyH.List)
	retrieveSurvey := requireUserMw.ApplyFn(surveyH.Retrieve)
	updateSurvey := requireUserMw.ApplyFn(surveyH.Update)

	r.HandleFunc("/surveys", createSurvey).Methods("POST")
	r.HandleFunc("/surveys", listSurveys).Methods("GET")
	r.HandleFunc("/surveys/{id}", retrieveSurvey).Methods("GET")
	r.HandleFunc("/surveys/{id}", updateSurvey).Methods("PUT")

	// Survey instant
	createSurveyInstanceSecured := requireUserMw.ApplyFn(handlers.CreateSurveyInstance(database))
	getSurveyInstancesSecured := handlers.GetSurveyInstances(database)
	getSingleSurveyInstance := handlers.GetSingleSurveyInstances(database)
	getSurveyInstanceResultSecured := handlers.GetSurveyInstanceResult(database)
	deleteSurveyInstanceResultSecured := requireUserMw.ApplyFn(handlers.DeleteSurveyInstanceResult(database))
	updateSurveyResponse := handlers.UpdateSurveyResponse(database)
	addSurveyFeedback := handlers.AddSurveyFeedback(database)
	getInstanceGroupsSecured := handlers.GetInstanceGroups(database)
	getInstanceTeamsSecured := handlers.GetInstanceTeams(database)

	r.HandleFunc("/surveys/{id}/instances", getSurveyInstancesSecured).Methods("GET")
	r.HandleFunc("/surveys/{id}/instances", createSurveyInstanceSecured).Methods("POST")
	r.HandleFunc("/surveys/{id}/instances/{instanceId}", getSurveyInstanceResultSecured).Methods("GET")
	r.HandleFunc("/surveys/{id}/instances/{instanceId}", deleteSurveyInstanceResultSecured).Methods("DELETE")
	r.HandleFunc("/survey/{id}", getSingleSurveyInstance).Methods("GET")
	r.HandleFunc("/survey/{id}", updateSurveyResponse).Methods("PUT")
	r.HandleFunc("/survey/{id}/feedbacks", addSurveyFeedback).Methods("POST")
	r.HandleFunc("/surveys/{id}/instance-groups", getInstanceGroupsSecured).Methods("GET")
	r.HandleFunc("/surveys/{id}/instance-teams", getInstanceTeamsSecured).Methods("GET")

	// Artifacts
	getArtifactsSecured := requireUserMw.ApplyFn(handlers.GetArtifacts(database))
	uploadArtifactSecured := requireUserMw.ApplyFn(handlers.UploadArtifact(database))
	getArtifactSecured := requireUserMw.ApplyFn(handlers.GetArtifact(database))

	r.HandleFunc("/dojo/{id}/artifacts", getArtifactsSecured).Methods("GET")
	r.HandleFunc("/dojo/{id}/artifacts", uploadArtifactSecured).Methods("POST")
	r.HandleFunc("/dojo/{id}/artifacts/{artifactId}", getArtifactSecured).Methods("GET")

	// Main route
	http.Handle("/", r)
	srv := &http.Server{
		Handler: r,
		Addr:    ":8000",
		// Good practice: enforce timeouts for servers you create!
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}
	log.Fatal(srv.ListenAndServe())
}
