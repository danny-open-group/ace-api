#!/bin/bash

CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o thesign/ace-api .

CMD='docker build -t thesign/ace-api -f ./smallDockerfile .'
echo '$ ' $CMD
$CMD
RET=$?
if [[ $RET -ne 0 ]]; then
    echo 'FAIL' $CMD ' ==> returned ' $RET
    exit 1
fi

CMD='docker login'
echo '$ ' $CMD
#$CMD
RET=$?
if [[ $RET -ne 0 ]]; then
    echo 'FAIL' $CMD ' ==> returned ' $RET
    exit 1
fi

CMD='docker push quartermill/ace-api'
echo '$ ' $CMD
#$CMD
RET=$?
if [[ $RET -ne 0 ]]; then
    echo 'FAIL' $CMD ' ==> returned ' $RET
    exit 1
fi
