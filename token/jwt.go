package token

import (
	"fmt"

	"crypto/rand"
	"encoding/base64"

	"github.com/dgrijalva/jwt-go"
)

var (
	RememberTokenBytes = 32
	SigningKey         = []byte("ace-super-secret-key")
)

func GetRemember(tokenString string) (remember string, err error) {
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return remember, fmt.Errorf("Unexpected signingmethod: %v", token.Header["alg"])
		}

		return SigningKey, nil
	})

	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		return claims["remember"].(string), nil
	} else {
		return remember, fmt.Errorf("Could not read token claims: %v", err)
	}
}

func JwtToken(name string, rememberToken string) (string, error) {
	token := jwt.New(jwt.SigningMethodHS256)

	claims := token.Claims.(jwt.MapClaims)
	claims["username"] = name
	claims["remember"] = rememberToken

	return token.SignedString(SigningKey)
}

func RememberToken() (string, error) {
	return String(RememberTokenBytes)
}

func Bytes(n int) ([]byte, error) {
	b := make([]byte, n)
	_, err := rand.Read(b)
	if err != nil {
		return nil, err
	}

	return b, nil
}

func String(nBytes int) (string, error) {
	b, err := Bytes(nBytes)
	if err != nil {
		return "", err
	}

	return base64.URLEncoding.EncodeToString(b), nil
}
