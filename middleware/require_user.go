package middleware

import (
	"fmt"
	"net/http"

	"gitlab.com/ace-api/context"
	"gitlab.com/ace-api/models"
)

type RequireUser struct {
	models.SessionService
}

func (mw *RequireUser) ApplyFn(next http.HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		token := r.Header.Get("Authorization")
		fmt.Println("TOKEN: ", token)
		if token == "" {
			if cookie, err := r.Cookie("jwt"); err == nil {
				token = cookie.Value
			}

			if token == "" {
				http.Error(w, "no authorization token provided", http.StatusUnauthorized)
				return
			}
		}

		session, err := mw.ByToken(token)
		if err != nil {
			http.Error(w, err.Error(), http.StatusUnauthorized)
			return
		}
		fmt.Println("Session found: ", session)

		ctx := r.Context()
		ctx = context.WithSession(ctx, session)
		r = r.WithContext(ctx)
		next(w, r)
	})
}

func (mw *RequireUser) Apply(next http.Handler) http.HandlerFunc {
	return mw.ApplyFn(next.ServeHTTP)
}
