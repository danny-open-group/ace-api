package handlers

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	"gitlab.com/ace-api/models"
	"net/http"
)

type Teams struct {
	ts models.TeamService
}

func NewTeams(ts models.TeamService) *Teams {
	return &Teams{
		ts: ts,
	}
}

func (t *Teams) Create(w http.ResponseWriter, r *http.Request) {
	var team models.Team

	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&team)
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusServiceUnavailable)
		return
	}

	log.Infoln("Incoming request...")
	err = t.ts.Create(&team)
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusServiceUnavailable)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	encoder := json.NewEncoder(w)
	err = encoder.Encode(team)
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusServiceUnavailable)
		return
	}
}

func (t *Teams) List(w http.ResponseWriter, r *http.Request) {
	log.Infoln("Incoming request...")
	teams, err := t.ts.List()
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusServiceUnavailable)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	encoder := json.NewEncoder(w)
	err = encoder.Encode(teams)
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusServiceUnavailable)
	}
}

func (t *Teams) Retrieve(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	results, err := t.ts.Retrieve(vars["id"])
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusServiceUnavailable)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	encoder := json.NewEncoder(w)
	err = encoder.Encode(results)
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusServiceUnavailable)
	}
}

func (t *Teams) Update(w http.ResponseWriter, r *http.Request) {
	var team models.Team

	vars := mux.Vars(r)
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&team)
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusServiceUnavailable)
		return
	}

	results, err := t.ts.Update(vars["id"], team)
	if err != nil {
		log.Error(err)
		switch err {
		case models.ErrDojosOpen:
			w.WriteHeader(http.StatusUnprocessableEntity)
			fmt.Fprintln(w, "{\"error\": \"All dojos must be closed\"}")
		case models.ErrChallengesMissing:
			w.WriteHeader(http.StatusUnprocessableEntity)
			fmt.Fprintln(w, "{\"error\": \"A team must have at least one challenge.\"}")
		default:
			w.WriteHeader(http.StatusServiceUnavailable)
		}
		return
	}

	w.Header().Set("Content-Type", "application/json")
	encoder := json.NewEncoder(w)
	err = encoder.Encode(results)
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusServiceUnavailable)
	}
}

func (t *Teams) AllDojos(w http.ResponseWriter, r *http.Request) {
	log.Infoln("Incoming request...")
	teams, err := t.ts.AllDojos()
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusServiceUnavailable)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	encoder := json.NewEncoder(w)
	err = encoder.Encode(teams)
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusServiceUnavailable)
	}
}

func (t *Teams) AddRating(w http.ResponseWriter, r *http.Request) {
	var rating models.Rating

	vars := mux.Vars(r)
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&rating)
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusServiceUnavailable)
		return
	}

	err = t.ts.AddRating(vars["id"], vars["dojoI"], rating)
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusServiceUnavailable)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	encoder := json.NewEncoder(w)
	err = encoder.Encode(rating)
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusServiceUnavailable)
	}
}
