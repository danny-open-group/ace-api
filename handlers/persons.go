package handlers

import (
	"encoding/json"
	"net/http"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	"gitlab.com/ace-api/models"
)

type Persons struct {
	ps models.PersonService
}

func NewPersons(ps models.PersonService) *Persons {
	return &Persons{
		ps: ps,
	}
}

func (p *Persons) Create(w http.ResponseWriter, r *http.Request) {
	var person models.Person

	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&person)
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusServiceUnavailable)
		return
	}

	log.Infoln("Incoming request...")
	err = p.ps.Create(&person)
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusServiceUnavailable)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	encoder := json.NewEncoder(w)
	err = encoder.Encode(person)
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusServiceUnavailable)
		return
	}
}

func (p *Persons) List(w http.ResponseWriter, r *http.Request) {
	log.Infoln("Incoming request...")
	persons, err := p.ps.List()
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusServiceUnavailable)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	encoder := json.NewEncoder(w)
	err = encoder.Encode(persons)
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusServiceUnavailable)
	}
}

func (p *Persons) Retrieve(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	results, err := p.ps.Retrieve(vars["id"])
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusServiceUnavailable)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	encoder := json.NewEncoder(w)
	err = encoder.Encode(results)
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusServiceUnavailable)
	}
}

func (p *Persons) Update(w http.ResponseWriter, r *http.Request) {
	var person models.Person

	vars := mux.Vars(r)
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&person)
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusServiceUnavailable)
		return
	}

	results, err := p.ps.Update(vars["id"], person)
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusServiceUnavailable)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	encoder := json.NewEncoder(w)
	err = encoder.Encode(results)
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusServiceUnavailable)
	}
}
