package handlers

import (
	"encoding/json"
	"net/http"

	log "github.com/sirupsen/logrus"

	"github.com/gorilla/mux"
	"gitlab.com/ace-api/store"
	"go.mongodb.org/mongo-driver/mongo"
)

func GetArtifacts(db *mongo.Database) http.HandlerFunc {
	return func(writer http.ResponseWriter, request *http.Request) {
		vars := mux.Vars(request)
		dojoID := vars["id"]

		if artifacts, err := store.GetArtifacts(dojoID, db); err != nil {
			log.Error(err)
			writer.WriteHeader(http.StatusInternalServerError)
			return
		} else {
			writer.Header().Set("Content-Type", "application/json")
			encoder := json.NewEncoder(writer)
			if err := encoder.Encode(artifacts); err != nil {
				writer.WriteHeader(http.StatusInternalServerError)
			}
		}
	}
}
