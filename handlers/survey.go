package handlers

import (
	"encoding/json"
	"net/http"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	"gitlab.com/ace-api/models"
)

type Surveys struct {
	ss models.SurveyService
}

func NewSurveys(ss models.SurveyService) *Surveys {
	return &Surveys{
		ss: ss,
	}
}

func (s *Surveys) Create(w http.ResponseWriter, r *http.Request) {

	decoder := json.NewDecoder(r.Body)

	var survey models.Survey
	if err := decoder.Decode(&survey); err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	log.Infoln("Incoming request...")
	if err := s.ss.Create(&survey); err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	encoder := json.NewEncoder(w)
	if err := encoder.Encode(survey); err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}

func (s *Surveys) AddResponse(w http.ResponseWriter, r *http.Request) {
	var survey models.Survey

	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&survey)
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusServiceUnavailable)
		return
	}

	log.Infoln("Incoming request...")
	err = s.ss.Create(&survey)
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusServiceUnavailable)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	encoder := json.NewEncoder(w)
	err = encoder.Encode(survey)
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusServiceUnavailable)
		return
	}
}

func (s *Surveys) List(w http.ResponseWriter, r *http.Request) {
	log.Infoln("Incoming request...")
	surveys, err := s.ss.List()
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	encoder := json.NewEncoder(w)
	if err = encoder.Encode(surveys); err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusInternalServerError)
	}
}

func (s *Surveys) Retrieve(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	results, err := s.ss.Retrieve(vars["id"])
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	encoder := json.NewEncoder(w)
	if err = encoder.Encode(results); err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusInternalServerError)
	}
}

func (s *Surveys) Update(w http.ResponseWriter, r *http.Request) {
	var survey models.Survey

	vars := mux.Vars(r)
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&survey); err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	results, err := s.ss.Update(vars["id"], survey)
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	encoder := json.NewEncoder(w)
	if err = encoder.Encode(results); err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusInternalServerError)
	}
}
