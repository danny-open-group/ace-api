package handlers

import (
	"encoding/json"
	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	"gitlab.com/ace-api/models"
	"net/http"
)

type Settings struct {
	ts models.SettingsService
}

func NewSettings(ts models.SettingsService) *Settings {
	return &Settings{
		ts: ts,
	}
}

func (t *Settings) SetSetting(w http.ResponseWriter, r *http.Request) {
	var setting models.Setting

	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&setting)
	print("1setting " + setting.Key + "|" + setting.Value)
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusServiceUnavailable)
		return
	}

	log.Infoln("Incoming request...")
	err = t.ts.SetSetting(&setting)
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusServiceUnavailable)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	encoder := json.NewEncoder(w)
	err = encoder.Encode(setting)
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusServiceUnavailable)
		return
	}
}

func (t *Settings) ListSettings(w http.ResponseWriter, r *http.Request) {
	log.Infoln("Incoming request...")
	settings, err := t.ts.ListSettings()
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusServiceUnavailable)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	encoder := json.NewEncoder(w)
	err = encoder.Encode(settings)
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusServiceUnavailable)
	}
}

func (t *Settings) DeleteSetting(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	key := vars["key"]

	log.Infoln("Incoming request...")
	err := t.ts.DeleteSetting(key)
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusServiceUnavailable)
		return
	}

}

func (t *Settings) GetSetting(w http.ResponseWriter, r *http.Request) {
	log.Infoln("Incoming request...")
	vars := mux.Vars(r)
	key := vars["key"]

	settings, err := t.ts.GetSetting(key)
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusServiceUnavailable)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	encoder := json.NewEncoder(w)
	err = encoder.Encode(settings)
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusServiceUnavailable)
	}
}
