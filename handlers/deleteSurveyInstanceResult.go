package handlers

import (
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/ace-api/store"
	"go.mongodb.org/mongo-driver/mongo"
)

func DeleteSurveyInstanceResult(db *mongo.Database) http.HandlerFunc {
	return func(writer http.ResponseWriter, request *http.Request) {
		vars := mux.Vars(request)
		surveyID := vars["id"]
		instanceID := vars["instanceId"]
		if err := store.DeleteSurveyInstance(surveyID, instanceID, db); err != nil {
			writer.WriteHeader(http.StatusInternalServerError)
		}
	}
}
