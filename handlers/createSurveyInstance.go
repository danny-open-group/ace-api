package handlers

import (
	"encoding/json"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/ace-api/store"
	"go.mongodb.org/mongo-driver/mongo"
)

type createSurveyInstanceRequest struct {
	Description string `json:"description,omitempty"`
	Group       string `json:"group"`
	Team        string `json:"team"`
	Quarter     string `json:"quarter"`
	From        string `json:"from"`
	To          string `json:"to"`
}

type createSurveyInsanceResponse struct {
	ID string `json:"id"`
}

// CreateSurveyInstance to create a survey instance
func CreateSurveyInstance(db *mongo.Database) http.HandlerFunc {
	return func(writer http.ResponseWriter, request *http.Request) {
		decoder := json.NewDecoder(request.Body)

		var surveyInstance createSurveyInstanceRequest
		if err := decoder.Decode(&surveyInstance); err != nil {
			writer.WriteHeader(http.StatusBadRequest)
			return
		}

		vars := mux.Vars(request)
		surveyID := vars["id"]
		survey, err := store.GetSurvey(surveyID, db)
		if err != nil {
			writer.WriteHeader(http.StatusInternalServerError)
			return
		}

		newSurveyInstance := store.CreateSurveyInstanceModel{
			Answers:     make([][]int, 0),
			Date:        survey.Date,
			Description: surveyInstance.Description,
			From:        surveyInstance.From,
			Questions:   survey.Questions,
			Subtitle:    survey.Subtitle,
			SurveyID:    survey.ID,
			Group:       surveyInstance.Group,
			Team:        surveyInstance.Team,
			Quarter:     surveyInstance.Quarter,
			Title:       survey.Title,
			To:          surveyInstance.To,
			Type:        survey.Type,
			Version:     survey.Version}

		if newID, err := store.CreateSurveyInstanceDoc(newSurveyInstance, db); err != nil {
			writer.WriteHeader(http.StatusInternalServerError)
			return
		} else {
			writer.Header().Set("Content-Type", "application/json")
			encoder := json.NewEncoder(writer)
			response := createSurveyInsanceResponse{newID}
			if err := encoder.Encode(response); err != nil {
				writer.WriteHeader(http.StatusInternalServerError)
				return
			}
		}
	}
}
