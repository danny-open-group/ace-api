package handlers

import (
	"encoding/json"
	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	"gitlab.com/ace-api/models"
	"net/http"
)

type DojoValues struct {
	ts models.DojoValuesService
}

func NewDojoValues(ts models.DojoValuesService) *DojoValues {
	return &DojoValues{
		ts: ts,
	}
}

func (t *DojoValues) SetDojoValue(w http.ResponseWriter, r *http.Request) {
	var dojoValue models.DojoValue

	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&dojoValue)
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusServiceUnavailable)
		return
	}

	log.Infoln("Incoming request...")
	err = t.ts.Create(&dojoValue)
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusServiceUnavailable)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	encoder := json.NewEncoder(w)
	err = encoder.Encode(dojoValue)
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusServiceUnavailable)
		return
	}
}

func (t *DojoValues) ListDojoValues(w http.ResponseWriter, r *http.Request) {
	log.Infoln("Incoming request...")
	vars := mux.Vars(r)
	dojoID := vars["dojoID"]

	dojoValues, err := t.ts.List(dojoID)
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusServiceUnavailable)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	encoder := json.NewEncoder(w)
	err = encoder.Encode(dojoValues)
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusServiceUnavailable)
	}
}
