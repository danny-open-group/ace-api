package handlers

import (
	"encoding/json"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/ace-api/store"
	"go.mongodb.org/mongo-driver/mongo"
)

type updateSurveyResponse struct {
	Answers []int `json:"answers"`
}

func UpdateSurveyResponse(db *mongo.Database) http.HandlerFunc {
	return func(writer http.ResponseWriter, request *http.Request) {
		decoder := json.NewDecoder(request.Body)

		var payload updateSurveyResponse
		if err := decoder.Decode(&payload); err != nil {
			writer.WriteHeader(http.StatusBadRequest)
			return
		}

		vars := mux.Vars(request)
		surveyInstanceID := vars["id"]
		if survey, err := store.GetSurveyInstance(surveyInstanceID, db); err != nil {
			writer.WriteHeader(http.StatusInternalServerError)
			return
		} else {
			if len(survey.Questions) != len(payload.Answers) {
				writer.WriteHeader(http.StatusBadRequest)
				return
			}
			if err := store.UpdateSurveyResponse(surveyInstanceID, payload.Answers, db); err != nil {
				writer.WriteHeader(http.StatusInternalServerError)
				return
			}
		}
	}
}
