package handlers

import (
	"encoding/json"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/ace-api/store"
	"go.mongodb.org/mongo-driver/mongo"
)

type getSurveyInsancesResponse struct {
	Instances []store.SurveyInstanceModel `json:"instances"`
}

func GetSurveyInstances(db *mongo.Database) http.HandlerFunc {
	return func(writer http.ResponseWriter, request *http.Request) {
		vars := mux.Vars(request)
		surveyID := vars["id"]

		queryParams := request.URL.Query()
		groups, hasGroup := queryParams["group"]
		var group string
		if hasGroup {
			group = groups[0]
		}
		teams, hasTeam := queryParams["team"]
		var team string
		if hasTeam {
			team = teams[0]
		}

		if surveys, err := store.GetSurveyInstances(surveyID, group, team, db); err != nil {
			writer.WriteHeader(http.StatusInternalServerError)
			return
		} else {
			writer.Header().Set("Content-Type", "application/json")
			encoder := json.NewEncoder(writer)
			var response getSurveyInsancesResponse
			if surveys == nil {
				response = getSurveyInsancesResponse{[]store.SurveyInstanceModel{}}
			} else {
				response = getSurveyInsancesResponse{surveys}
			}
			if err := encoder.Encode(response); err != nil {
				writer.WriteHeader(http.StatusInternalServerError)
			}
		}

	}
}
