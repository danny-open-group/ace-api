package handlers

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	"gitlab.com/ace-api/models"
)

type Dojos struct {
	ts       models.DojoService
	services *models.Services
}

func NewDojos(ts models.DojoService, services *models.Services) *Dojos {
	return &Dojos{
		ts:       ts,
		services: services,
	}
}

func (t *Dojos) Create(w http.ResponseWriter, r *http.Request) {
	var dojo models.Dojo

	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&dojo)
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusServiceUnavailable)
		return
	}

	log.Infoln("Incoming request...")
	err = t.ts.Create(&dojo)
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusServiceUnavailable)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	encoder := json.NewEncoder(w)
	err = encoder.Encode(dojo)
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusServiceUnavailable)
		return
	}
}

func (t *Dojos) List(w http.ResponseWriter, r *http.Request) {
	log.Infoln("Incoming request...")
	dojos, err := t.ts.List()
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusServiceUnavailable)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	encoder := json.NewEncoder(w)
	err = encoder.Encode(dojos)
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusServiceUnavailable)
	}
}

func (t *Dojos) Retrieve(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	results, err := t.ts.Retrieve(vars["id"])
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusServiceUnavailable)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	encoder := json.NewEncoder(w)
	err = encoder.Encode(results)
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusServiceUnavailable)
	}
}

func (t *Dojos) Update(w http.ResponseWriter, r *http.Request) {
	var dojo models.Dojo

	vars := mux.Vars(r)
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&dojo)
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusServiceUnavailable)
		return
	}

	results, err := t.ts.Update(vars["id"], dojo)
	if err != nil {
		log.Error(err)
		switch err {
		case models.ErrDojosOpen:
			w.WriteHeader(http.StatusUnprocessableEntity)
			fmt.Fprintln(w, "{\"error\": \"All dojos must be closed\"}")
		case models.ErrChallengesMissing:
			w.WriteHeader(http.StatusUnprocessableEntity)
			fmt.Fprintln(w, "{\"error\": \"A dojo must have at least one challenge.\"}")
		default:
			w.WriteHeader(http.StatusServiceUnavailable)
		}
		return
	}

	w.Header().Set("Content-Type", "application/json")
	encoder := json.NewEncoder(w)
	err = encoder.Encode(results)
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusServiceUnavailable)
	}
}

func (t *Dojos) AllDojos(w http.ResponseWriter, r *http.Request) {
	log.Infoln("Incoming request...")
	dojos, err := t.ts.AllDojos()
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusServiceUnavailable)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	encoder := json.NewEncoder(w)
	err = encoder.Encode(dojos)
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusServiceUnavailable)
	}
}

func (t *Dojos) AddRating(w http.ResponseWriter, r *http.Request) {
	var rating models.Rating

	vars := mux.Vars(r)
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&rating)
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusServiceUnavailable)
		return
	}

	err = t.ts.AddRating(vars["id"], vars["id"], rating)
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusServiceUnavailable)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	encoder := json.NewEncoder(w)
	err = encoder.Encode(rating)
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusServiceUnavailable)
	}
}
