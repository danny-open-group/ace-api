package handlers

import (
	"io/ioutil"
	"net/http"

	log "github.com/sirupsen/logrus"

	"github.com/gorilla/mux"
	"gitlab.com/ace-api/store"
	"go.mongodb.org/mongo-driver/mongo"
)

const maxUploadSize = 15 * 1024 * 1024

func UploadArtifact(db *mongo.Database) http.HandlerFunc {
	return func(writer http.ResponseWriter, request *http.Request) {
		request.Body = http.MaxBytesReader(writer, request.Body, maxUploadSize)
		if err := request.ParseMultipartForm(maxUploadSize); err != nil {
			log.Error("Parse", err)
			writer.WriteHeader(http.StatusBadRequest)
			return
		}

		fileName := request.PostFormValue("name")
		file, _, err := request.FormFile("file")
		if err != nil {
			log.Error("FormFile", err)
			writer.WriteHeader(http.StatusBadRequest)
			return
		}
		defer file.Close()

		fileBytes, err := ioutil.ReadAll(file)
		if err != nil {
			log.Error("ReadAll", err)
			writer.WriteHeader(http.StatusBadRequest)
			return
		}

		fileType := http.DetectContentType(fileBytes)

		vars := mux.Vars(request)
		dojoID := vars["id"]

		if err := store.SaveArtifact(dojoID, fileName, fileType, fileBytes, db); err != nil {
			log.Error(err)
			writer.WriteHeader(http.StatusInternalServerError)
			return
		}

	}
}
