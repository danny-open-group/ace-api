package handlers

import (
	"encoding/json"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/ace-api/models"
	"gitlab.com/ace-api/store"
	"go.mongodb.org/mongo-driver/mongo"
)

type getSingleSurveyInstancesResultResponse struct {
	Title     string            `json:"title"`
	Subtitle  string            `json:"subtitle"`
	Group     string            `json:"group"`
	Team      string            `json:"team"`
	Quarter   string            `json:"quarter"`
	To        string            `json:"to"`
	From      string            `json:"from"`
	Questions []models.Question `json:"questions"`
	Answers   [][]int           `json:"answers"`
}

func GetSurveyInstanceResult(db *mongo.Database) http.HandlerFunc {
	return func(writer http.ResponseWriter, request *http.Request) {
		vars := mux.Vars(request)
		instanceID := vars["instanceId"]
		if surveyInstance, err := store.GetSurveyInstance(instanceID, db); err != nil {
			writer.WriteHeader(http.StatusBadRequest)
		} else {
			writer.Header().Set("Content-Type", "application/json")
			encoder := json.NewEncoder(writer)
			response := getSingleSurveyInstancesResultResponse{
				Title:     surveyInstance.Title,
				Subtitle:  surveyInstance.Type,
				Group:     surveyInstance.Group,
				Team:      surveyInstance.Team,
				Quarter:   surveyInstance.Quarter,
				From:      surveyInstance.From,
				To:        surveyInstance.To,
				Questions: surveyInstance.Questions,
				Answers:   surveyInstance.Answers}
			if err := encoder.Encode(response); err != nil {
				writer.WriteHeader(http.StatusInternalServerError)
			}
		}
	}
}
