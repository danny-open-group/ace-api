package handlers

import (
	"encoding/json"
	"net/http"

	"gitlab.com/ace-api/store"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	"go.mongodb.org/mongo-driver/mongo"
)

func GetInstanceGroups(db *mongo.Database) http.HandlerFunc {
	return func(writer http.ResponseWriter, request *http.Request) {
		vars := mux.Vars(request)
		surveyID := vars["id"]
		if groups, err := store.GetSurveyInstanceGroups(surveyID, db); err != nil {
			log.Error(err)
			writer.WriteHeader(http.StatusInternalServerError)
			return
		} else {
			writer.Header().Set("Content-Type", "application/json")
			encoder := json.NewEncoder(writer)
			err = encoder.Encode(groups)
			if err != nil {
				log.Error(err)
				writer.WriteHeader(http.StatusInternalServerError)
			}
		}
	}
}
