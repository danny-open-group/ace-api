package handlers

import (
	"encoding/json"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/ace-api/models"
	"gitlab.com/ace-api/store"
	"go.mongodb.org/mongo-driver/mongo"
)

type getSingleSurveyInstancesResponse struct {
	Title     string            `json:"title"`
	Subtitle  string            `json:"subtitle"`
	Questions []models.Question `json:"questions"`
}

// GetSingleSurveyInstances returns an instant of survey based on the id provided
func GetSingleSurveyInstances(db *mongo.Database) http.HandlerFunc {
	return func(writer http.ResponseWriter, request *http.Request) {
		vars := mux.Vars(request)
		instanceID := vars["id"]
		if surveyInstance, err := store.GetSurveyInstance(instanceID, db); err != nil {
			writer.WriteHeader(http.StatusBadRequest)
			return
		} else {
			writer.Header().Set("Content-Type", "application/json")
			encoder := json.NewEncoder(writer)
			response := getSingleSurveyInstancesResponse{
				Title:     surveyInstance.Title,
				Subtitle:  surveyInstance.Type,
				Questions: surveyInstance.Questions}
			if err := encoder.Encode(response); err != nil {
				writer.WriteHeader(http.StatusInternalServerError)
			}
		}
	}
}
