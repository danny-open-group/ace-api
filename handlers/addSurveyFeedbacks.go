package handlers

import (
	"encoding/json"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	"gitlab.com/ace-api/store"
	"go.mongodb.org/mongo-driver/mongo"
)

type addSurveyFeedbackPayload struct {
	Rating   int    `json:"rating,omitempty"`
	Feedback string `json:"feedback"`
}

func AddSurveyFeedback(db *mongo.Database) http.HandlerFunc {
	return func(writer http.ResponseWriter, request *http.Request) {
		decoder := json.NewDecoder(request.Body)

		var payload addSurveyFeedbackPayload
		if err := decoder.Decode(&payload); err != nil {
			writer.WriteHeader(http.StatusBadRequest)
			return
		}

		vars := mux.Vars(request)
		surveyInstanceID := vars["id"]
		if survey, err := store.GetSurveyInstance(surveyInstanceID, db); err != nil {
			writer.WriteHeader(http.StatusInternalServerError)
			return
		} else {
			now := time.Now()
			doc := store.SurveyFeedbackModel{
				SurveyID:   survey.SurveyID,
				InstanceID: survey.ID,
				Rating:     payload.Rating,
				Feedback:   payload.Feedback,
				Time:       &now}
			if err := store.AddSurveyFeedback(doc, db); err != nil {
				writer.WriteHeader(http.StatusInternalServerError)
			}
		}
	}
}
