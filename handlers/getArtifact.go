package handlers

import (
	"net/http"

	log "github.com/sirupsen/logrus"

	"github.com/gorilla/mux"
	"gitlab.com/ace-api/store"
	"go.mongodb.org/mongo-driver/mongo"
)

func GetArtifact(db *mongo.Database) http.HandlerFunc {
	return func(writer http.ResponseWriter, request *http.Request) {
		vars := mux.Vars(request)
		dojoID := vars["id"]
		artifactID := vars["artifactId"]

		if artifact, err := store.GetArtifact(dojoID, artifactID, db); err != nil {
			log.Error(err)
			writer.WriteHeader(http.StatusInternalServerError)
			return
		} else {
			writer.Header().Set("Content-Type", artifact.FileType)
			writer.Write(artifact.Mime)
		}
	}
}
