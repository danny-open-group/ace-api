#!/bin/bash

CMD='docker build -t quartermill/ace-api .'
echo '$ ' $CMD
$CMD
RET=$?
if [[ $RET -ne 0 ]]; then
    echo 'FAIL' $CMD ' ==> returned ' $RET
    exit 1
fi

CMD='docker login'
echo '$ ' $CMD
$CMD
RET=$?
if [[ $RET -ne 0 ]]; then
    echo 'FAIL' $CMD ' ==> returned ' $RET
    exit 1
fi

CMD='docker push quartermill/ace-api'
echo '$ ' $CMD
$CMD
RET=$?
if [[ $RET -ne 0 ]]; then
    echo 'FAIL' $CMD ' ==> returned ' $RET
    exit 1
fi