# Notes for ACE API

## MongoDB instance in docker
`$ docker run --name mongo42 -d -p 27017:27017 mongo:4.2`

### NOTE:  
The app connects to mongo42:27017; so if you want to use the app locally and the database in a container, you need to add the following line to `/etc/hosts`:  
`127.0.0.1     mongo42`  
If you get an error like this:  
`level=error msg="Error populating database: server selection error: server selection timeout" [...] `  
ensure you have completed the above step **first** and then:  
`$ docker stop mongo42`  
`$ docker start mongo42`  

## Load GoLang dependencies
Install [go-dep](https://golang.github.io/dep/):

```bash
$ dep ensure
```

## Run Go Service

```bash
$ cd ~/go/src/gitlab.com/ace-api
$ go run main.go
```

## Hit running service

```bash
$ curl  http://localhost:8000
```

## Build docker image

```bash
$ docker build -t "quartermill/ace-api" .
```

## Run docker image

```bash
$ docker run -it --rm --name run-ace-api --link mongo42:mongo42 -p 8000:8000 ace-api
```

## Run shell on existing docker image

```bash
$ docker exec -it run-ace-api bash
```

# Deployment Notes

```bash
# ace-api stuff
$ docker login   
$ docker push quartermill/ace-api
$ docker logout
```


# Commands from the Google Cloud Platform (GCP) server

```bash
# Public IP   34.67.161.7

# Setup network (only once)
$ sudo docker network create ace-net

# Run Mongo (only once)
$ sudo docker run --name mongo42 -d -p 27017:27017 --network ace-net mongo:4.2

# build network (only once)
$ docker network create ace-net

# build ace-api image
$ cd $REPO_PATH/ace-api
# Make sure you're on the right branch
$ docker pull    
$ docker build -t "quartermill/ace-api" .

# push completed images to dockerhub
$ sudo docker login
$ sudo docker push quartermill/ace-api
$ sudo docker push quartermill/ace-ui
$ sudo docker logout


# Pull down ace images from dockerhub
$ sudo docker login
$ sudo docker pull quartermill/ace-api
$ sudo docker pull quartermill/ace-ui
$ sudo docker logout

# Run ace-api instance 
$ sudo docker run -d --name ace-api --link mongo42:mongo42 --network ace-net -p 8000:8000 quartermill/ace-api

# Run ace-ui instance
sudo docker run -d -e "CADDYPATH=/etc/caddycerts" -v /etc/caddycerts:/etc/caddycerts -p 80:80 -p 443:443 --name ace-ui --network ace-net quartermill/ace-ui

```
