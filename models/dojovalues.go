package models

import (
	"context"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type DojoValue struct {
	ID               primitive.ObjectID `bson:"_id,omitempty" json:"id"`
	Date             time.Time          `bson:"date" json:"date"`
	DojoID           string             `bson:"dojoid" json:"dojoid"`
	Timeframe        int                `bson:"timeframe" json:"timeframe"`
	SameTeam         bool               `bson:"sameteam" json:"sameteam"`
	CleanCode        int                `bson:"cleancode" json:"cleancode"`
	Refactor         int                `bson:"refactor" json:"refactor"`
	UnitTesting      int                `bson:"unittesting" json:"unittesting"`
	TestDriven       int                `bson:"testdriven" json:"testdriven"`
	EndToEnd         int                `bson:"endtoend" json:"endtoend"`
	DoCD             int                `bson:"docd" json:"docd"`
	DoCI             int                `bson:"doci" json:"doci"`
	Pairing          int                `bson:"pairing" json:"pairing"`
	SkillsMatrix     int                `bson:"skillsmatrix" json:"skillsmatrix"`
	FocusedStandup   int                `bson:"focusedstandup" json:"focusedstandup"`
	Agreement        int                `bson:"agreement" json:"agreement"`
	Dod              int                `bson:"dod" json:"dod"`
	Dor              int                `bson:"dor" json:"dor"`
	Metrics          int                `bson:"metrics" json:"metrics"`
	Wip              int                `bson:"wip" json:"wip"`
	Innovation       int                `bson:"innovation" json:"innovation"`
	TeamBuilding     int                `bson:"teambuilding" json:"teambuilding"`
	Documentation    int                `bson:"documentation" json:"documentation"`
	JiraBoard        int                `bson:"jiraboard" json:"jiraboard"`
	Refinement       int                `bson:"refinement" json:"refinement"`
	TeamVision       int                `bson:"teamvision" json:"teamvision"`
	TeamMission      int                `bson:"teammission" json:"teammission"`
	ValueMeetings    int                `bson:"valuemeetings" json:"valuemeetings"`
	StoryMapping     int                `bson:"storymapping" json:"storymapping"`
	MobProgramming   int                `bson:"mobprogramming" json:"mobprogramming"`
	TeamSafety       int                `bson:"teamsafety" json:"teamsafety"`
	Retro            int                `bson:"retro" json:"retro"`
	PlanningSessions int                `bson:"planning" json:"planning"`
	Demo             int                `bson:"demo" json:"demo"`
	Roles            int                `bson:"roles" json:"roles"`
	Dependencies     int                `bson:"deps" json:"deps"`
	Enough           int                `bson:"enough" json:"enough"`
	PI               int                `bson:"pi" json:"pi"`
	CodeBreak        int                `bson:"codebreak" json:"codebreak"`
	Load             int                `bson:"load" json:"load"`
	Remote           int                `bson:"remote" json:"remote"`
	TeamHeard        int                `bson:"heard" json:"heard"`
	Communication    int                `bson:"comm" json:"comm"`
	TechStories      int                `bson:"techstories" json:"techstories"`
	Unplanned        int                `bson:"unplanned" json:"unplanned"`
	Git              int                `bson:"git" json:"git"`
	Logs             int                `bson:"logs" json:"logs"`
	TechStack        int                `bson:"techstack" json:"techstack"`
	//Comment          string             `bson:"comment" json:"comment"`
}

type DojoValuesDB interface {
	Create(dojoValue *DojoValue) error
	List(dojoID string) ([]DojoValue, error)
}

type DojoValuesService interface {
	DojoValuesDB
}

type dojoValuesService struct {
	DojoValuesDB
}

type dojoValuesMongo struct {
	db         *mongo.Database
	collection string
}

func (d dojoValuesMongo) Create(dojoValue *DojoValue) error {
	collection := d.db.Collection(d.collection)
	res, err := collection.InsertOne(context.Background(), dojoValue)
	if err != nil {
		return err
	}

	dojoValue.ID = res.InsertedID.(primitive.ObjectID)
	return nil
}

func (d dojoValuesMongo) List(dojoID string) ([]DojoValue, error) {
	dojoValues := make([]DojoValue, 0)

	collection := d.db.Collection(d.collection)
	filter := bson.M{"DojoId": dojoID}
	cursor, err := collection.Find(context.Background(), filter)
	if err != nil {
		return nil, err
	}

	err = cursor.All(context.Background(), &dojoValues)
	if err != nil {
		return nil, err
	}
	defer cursor.Close(context.Background())

	return dojoValues, err
}

var _ DojoValuesDB = &dojoValuesMongo{}

func NewDojoValuesService(db *mongo.Database) DojoValuesService {
	return &dojoValuesService{
		DojoValuesDB: &dojoValuesMongo{
			db:         db,
			collection: "dojovalues",
		},
	}
}
