package models

import (
	"context"
	"fmt"
	"time"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type Services struct {
	Team       TeamService
	Dojo       DojoService
	DojoValues DojoValuesService
	Setting    SettingsService
	User       UserService
	Person     PersonService
	Session    SessionService
	Survey     SurveyService
	client     *mongo.Client
}

func NewServices(connectionInfo, dbName string) (*Services, *mongo.Database, error) {
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(connectionInfo))
	if err != nil {
		return nil, nil, err
	}
	db := client.Database(dbName)

	services := &Services{
		Team:       NewTeamService(db),
		Dojo:       NewDojoService(db),
		DojoValues: NewDojoValuesService(db),
		Setting:    NewSettingsService(db),
		User:       NewUserService(db),
		Person:     NewPersonService(db),
		Survey:     NewSurveyService(db),
		Session:    NewSessionService(db),
		client:     client,
	}

	teams, err := services.Team.List()
	if err != nil {
		return nil, nil, fmt.Errorf("Error populating database: %v", err)
	}

	if len(teams) == 0 {
		err := services.populateDefaultTeams()
		if err != nil {
			return nil, nil, fmt.Errorf("Error populating database: %v", err)
		}
	}

	return services, db, nil
}

func (s *Services) Close() error {
	return s.client.Disconnect(context.Background())
}

func (s *Services) populateDefaultTeams() error {
	now := time.Now()

	teams := [...]Team{
		{Rank: 92,
			Name:     "Orange Juice",
			Location: "CiC",
			Leader: &Person{
				FirstName: "Brian Hand",
				Email:     "bhand@email.com",
			},
		},
		{Rank: 87,
			Name:     "Prune Juice",
			Location: "CiC",
			Leader: &Person{
				FirstName: "Maggie Hill",
				Email:     "bhand@email.com",
			},
		},
		{Rank: 82,
			Name:     "Bussel Sprout Juice",
			Location: "CiC",
			Leader: &Person{
				FirstName: "Yo-Yo  Ma-Ma",
				Email:     "bhand@email.com",
			},
		},
		{Rank: 95,
			Name:     "Blueberry Juice",
			Location: "CiC",
			Leader: &Person{
				FirstName: "Chereze Theron",
				Email:     "bhand@email.com",
			},
			Completed:  &now,
			DojoRating: 90,
		},
		{Rank: 85,
			Name:     "Pineapple Juice",
			Location: "CiC",
			Leader: &Person{
				FirstName: "Spider Man",
				Email:     "bhand@email.com",
			},
			Completed:  &now,
			DojoRating: 75,
		},
		{Rank: 75,
			Name:     "Mayberry Juice",
			Location: "CiC",
			Leader: &Person{
				FirstName: "Chereze Theron",
				Email:     "bhand@email.com",
			},
			Completed:  &now,
			DojoRating: 65,
		}}

	for _, team := range teams {
		err := s.Team.Create(&team)
		if err != nil {
			fmt.Errorf("problem creating initial teams: %s", err)
		}
	}

	return nil
}
