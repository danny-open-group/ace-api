package models

import (
	"context"
	"fmt"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type Person struct {
	ID        primitive.ObjectID `bson:"_id,omitempty" json:"id,omitempty"`
	FirstName string             `bson:"firstName" json:"firstName"`
	LastName  string             `bson:"lastName" json:"lastName"`
	Email     string             `bson:"email" json:"email"`
	TechStack string             `bson:"techStack" json:"techStack"`
}

type PersonDB interface {
	Create(person *Person) error
	List() ([]Person, error)
	Retrieve(id string) (Person, error)
	Update(id string, person Person) (interface{}, error)
}

type PersonService interface {
	PersonDB
}

type personService struct {
	PersonDB
}

type personValidator struct {
	PersonDB
}

type personMongo struct {
	db         *mongo.Database
	collection string
}

var _ PersonDB = &personMongo{}

func (tm *personMongo) Create(person *Person) error {
	collection := tm.db.Collection(tm.collection)
	res, err := collection.InsertOne(context.Background(), person)
	if err != nil {
		return err
	}

	person.ID = res.InsertedID.(primitive.ObjectID)
	return nil
}

func (tm *personMongo) List() ([]Person, error) {
	persons := make([]Person, 0)

	collection := tm.db.Collection(tm.collection)
	filter := bson.D{{}}
	cursor, err := collection.Find(context.Background(), filter)
	if err != nil {
		return nil, err
	}

	err = cursor.All(context.Background(), &persons)
	if err != nil {
		return nil, err
	}
	defer cursor.Close(context.Background())

	return persons, err
}

func (tm *personMongo) Retrieve(id string) (Person, error) {
	var person Person

	collection := tm.db.Collection(tm.collection)
	objID, err := primitive.ObjectIDFromHex(id)
	filter := bson.M{"_id": bson.M{"$eq": objID}}
	err = collection.FindOne(context.Background(), filter).Decode(&person)
	if err != nil {
		return person, err
	}

	return person, err
}

func (tm *personMongo) Update(id string, person Person) (interface{}, error) {
	collection := tm.db.Collection(tm.collection)
	objID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return nil, err
	}

	updatePerson := bson.M{"$set": person}
	filter := bson.M{"_id": bson.M{"$eq": objID}}
	res, err := collection.UpdateOne(context.Background(), filter, updatePerson)
	if err != nil {
		return nil, err
	}

	return res.UpsertedID, nil
}

func NewPersonService(db *mongo.Database) PersonService {
	return &personService{
		PersonDB: &personValidator{
			PersonDB: &personMongo{
				db:         db,
				collection: "people",
			},
		},
	}
}

type personValFn func(*Person) error

func runPersonValFns(person *Person, fns ...personValFn) error {
	for _, fn := range fns {
		if err := fn(person); err != nil {
			return err
		}
	}
	return nil
}

func (pv *personValidator) nameRequired(person *Person) error {
	if person.FirstName == "" {
		return fmt.Errorf("First name is required")
	}
	if person.LastName == "" {
		return fmt.Errorf("Last name is required")
	}
	return nil
}

func (pv *personValidator) Create(person *Person) error {
	err := runPersonValFns(person, pv.nameRequired)
	if err != nil {
		return err
	}

	return pv.PersonDB.Create(person)
}
