package models

import (
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"time"
)

type Setting struct {
	Key        string `bson:"key" json:"key"`
	Value      string `bson:"value" json:"value"`
	LastUpdate time.Time
}

type SettingsDB interface {
	SetSetting(setting *Setting) error
	ListSettings() ([]Setting, error)
	DeleteSetting(key string) error
	GetSetting(key string) (*Setting, error)
	GetSettingValue(key string) string
}

type SettingsService interface {
	SettingsDB
}

type settingsService struct {
	SettingsDB
}

type settingsMongo struct {
	db         *mongo.Database
	collection string
}

var _ SettingsDB = &settingsMongo{}

func (settingsM *settingsMongo) SetSetting(setting *Setting) error {
	collection := settingsM.db.Collection(settingsM.collection)
	filter := bson.M{"_id": setting.Key}
	update := bson.M{
		"$set": setting,
	}
	print("Saving Setting: " + setting.Key + "|" + setting.Value)
	uo := options.MergeUpdateOptions()
	uo.SetUpsert(true)
	_, err := collection.UpdateOne(context.Background(), filter, update, uo)
	if err != nil {
		return err
	}
	return nil
}

func (settingsM *settingsMongo) GetSetting(key string) (*Setting, error) {
	settings := make([]Setting, 0)

	collection := settingsM.db.Collection(settingsM.collection)
	filter := bson.M{"_id": key}
	cursor, err := collection.Find(context.Background(), filter)
	if err != nil {
		return nil, err
	}

	err = cursor.All(context.Background(), &settings)
	if err != nil {
		return nil, err
	}
	defer cursor.Close(context.Background())

	return &settings[0], err
}

func (settingsM *settingsMongo) GetSettingValue(key string) string {
	settings := make([]Setting, 0)

	collection := settingsM.db.Collection(settingsM.collection)
	filter := bson.M{"_id": key}
	cursor, err := collection.Find(context.Background(), filter)
	if err != nil {
		return ""
	}

	err = cursor.All(context.Background(), &settings)
	if err != nil {
		return ""
	}
	defer cursor.Close(context.Background())

	if len(settings) == 0 {
		return ""
	}

	return settings[0].Value
}

func (settingsM *settingsMongo) ListSettings() ([]Setting, error) {
	settings := make([]Setting, 0)

	collection := settingsM.db.Collection(settingsM.collection)
	filter := bson.D{{}}
	cursor, err := collection.Find(context.Background(), filter)
	if err != nil {
		return nil, err
	}

	err = cursor.All(context.Background(), &settings)
	if err != nil {
		return nil, err
	}
	defer cursor.Close(context.Background())

	return settings, err
}

func (settingsM *settingsMongo) DeleteSetting(key string) error {
	collection := settingsM.db.Collection(settingsM.collection)
	filter := bson.D{{"key", key}}
	_, err := collection.DeleteOne(context.Background(), filter)
	if err != nil {
		return err
	}
	return nil
}

func NewSettingsService(db *mongo.Database) SettingsService {
	return &settingsService{
		SettingsDB: &settingsMongo{
			db:         db,
			collection: "settings",
		},
	}
}
