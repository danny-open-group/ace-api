package models

import (
	"context"
	"errors"
	"fmt"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

var (
	ErrSurveysQuestions = errors.New("modles: A survey must have one or more questions!")
)

type QuestionType struct {
	// types may include gradient (1-5) percent (0,25,50,75,100) boolean (0,1) thumbs (0,1)
	Code    string   `bson:"code" json:"code"`
	Name    string   `bson:"name" json:"name"`
	Label1  string   `bson:"label1" json:"label1"`
	Label2  string   `bson:"label2" json:"label2"`
	Type    string   `bson:"type" json:"type"`
	Answers []string `bson:"choices,omitempty" json:"choices,omitempty"`
}

type Question struct {
	Code     string    `bson:"code" json:"code"`
	Category string    `bson:"category" json:"category"`
	Title    string    `bson:"title" json:"title"`
	Text     string    `bson:"text" json:"text"`
	Type     string    `bson:"type" json:"type"`
	Date     time.Time `bson:"date" json:"date"`
	Answers  []int     `bson:"answer,omitempty" json:"answer,omitempty"`
}

type Survey struct {
	ID        primitive.ObjectID `bson:"_id,omitempty" json:"id,omitempty"`
	Type      string             `bson:"type" json:"type"`
	Version   string             `bson:"version" json:"version"`
	Title     string             `bson:"title" json:"title"`
	Subtitle  string             `bson:"subtitle" json:"subtitle"`
	Date      *time.Time         `bson:"date" json:"date"`
	Questions []Question         `bson:"questions" json:"questions"`
}

type SurveyDB interface {
	Create(survey *Survey) error
	List() ([]Survey, error)
	Retrieve(id string) (*Survey, error)
	AddResponse(id, surveyI string, rating Rating) error
	Update(id string, survey Survey) (interface{}, error)
}

type SurveyService interface {
	SurveyDB
}

type surveyService struct {
	SurveyDB
}

type surveyValidator struct {
	SurveyDB
}

type surveyMongo struct {
	db         *mongo.Database
	collection string
}

var _ SurveyDB = &surveyMongo{}

func (surveyM *surveyMongo) Create(survey *Survey) error {
	collection := surveyM.db.Collection(surveyM.collection)
	res, err := collection.InsertOne(context.Background(), survey)
	if err != nil {
		return err
	}

	survey.ID = res.InsertedID.(primitive.ObjectID)
	return nil
}

func (surveyM *surveyMongo) List() ([]Survey, error) {
	surveys := make([]Survey, 0)
	collection := surveyM.db.Collection(surveyM.collection)
	filter := bson.D{{}}
	var cursor *mongo.Cursor
	var err error
	if cursor, err = collection.Find(context.Background(), filter); err != nil {
		return nil, err
	}

	defer cursor.Close(context.Background())
	if err := cursor.All(context.Background(), &surveys); err != nil {
		return nil, err
	}

	return surveys, nil
}

func (surveyM *surveyMongo) Retrieve(id string) (*Survey, error) {
	oid, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return nil, err
	}

	filter := bson.M{"_id": oid}
	collection := surveyM.db.Collection(surveyM.collection)
	cursor, err := collection.Find(context.Background(), filter)
	if err != nil {
		return nil, err
	}

	defer cursor.Close(context.Background())

	var surveys []Survey
	if err = cursor.All(context.Background(), &surveys); err != nil {
		return nil, err
	}

	//TODO: handle 404 if survey is not found
	if len(surveys) == 0 {
		return nil, err
	}
	return &surveys[0], err
}

func (surveyM *surveyMongo) AddResponse(id string, surveyI string, rating Rating) error {
	objID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return err
	}

	rating.Date = time.Now()

	updateSurvey := bson.M{} // TODO
	filter := bson.M{"_id": bson.M{"$eq": objID}}
	collection := surveyM.db.Collection(surveyM.collection)
	if _, err = collection.UpdateOne(context.Background(), filter, updateSurvey); err != nil {
		return err
	}

	return nil
}

func (surveyM *surveyMongo) Update(id string, survey Survey) (interface{}, error) {
	objID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return nil, err
	}

	updateSurvey := bson.M{"$set": survey}
	filter := bson.M{"_id": bson.M{"$eq": objID}}
	collection := surveyM.db.Collection(surveyM.collection)
	res, err := collection.UpdateOne(context.Background(), filter, updateSurvey)
	if err != nil {
		return nil, err
	}

	return res.UpsertedID, nil
}

func NewSurveyService(db *mongo.Database) SurveyService {
	return &surveyService{
		SurveyDB: &surveyValidator{
			SurveyDB: &surveyMongo{
				db:         db,
				collection: "surveys",
			},
		},
	}
}

type surveyValFn func(*Survey) error

func runSurveyValFns(survey *Survey, fns ...surveyValFn) error {
	for _, fn := range fns {
		if err := fn(survey); err != nil {
			return err
		}
	}
	return nil
}

func (surveyV *surveyValidator) Update(id string, survey Survey) (interface{}, error) {
	fmt.Println("checking errors")

	if err := runSurveyValFns(&survey, surveyV.allocateArrays); err != nil {
		return nil, err
	}

	return surveyV.SurveyDB.Update(id, survey)
}

func (surveyV *surveyValidator) allocateArrays(survey *Survey) error {
	if survey.Questions == nil {
		survey.Questions = make([]Question, 0)
	}
	return nil
}

func (surveyV *surveyValidator) Create(survey *Survey) error {

	if err := runSurveyValFns(survey, surveyV.allocateArrays); err != nil {
		return err
	}

	return surveyV.SurveyDB.Create(survey)
}
