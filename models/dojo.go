package models

import (
	"context"
	"errors"
	"fmt"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

var (
	ErrDojosOpen = errors.New(
		"modles: A dojo should have no open dojo when marked completed.")
	ErrChallengesMissing = errors.New(
		"modles: A dojo requires challenges when marked completed.")
)

type Rating struct {
	Week      int       `bson:"week" json:"week"`
	Date      time.Time `bson:"date" json:"date"`
	Happiness bool      `bson:"happiness" json:"happiness"`
	Value     bool      `bson:"value" json:"value"`
	Safety    bool      `bson:"safety" json:"safety"`
	Comment   string    `bson:"comment" json:"comment"`
}

type Note struct {
	Title   string    `bson:"title" json:"title"`
	Content string    `bson:"content" json:"content"`
	Author  string    `bson:"author" json:"author"`
	Date    time.Time `bson:"date" json:"date"`
}

type Assessment struct {
	Date          string  `bson:"date"           json:"date"`
	Score         float64 `bson:"score"          json:"score"`
	CoreTech      string  `bson:"coreTech"       json:"coreTech"`
	UseTimberlake string  `bson:"useTimberlake"  json:"useTimberlake"`
}

type Dojo struct {
	ID         primitive.ObjectID `bson:"_id,omitempty" json:"id,omitempty"`
	Status     string             `bson:"status" json:"status"`
	Teams      []Team             `bson:"teams" json:"teams"`
	Ratings    []Rating           `bson:"ratings,omitempty" json:"ratings,omitempty"`
	DojoValues []DojoValue        `bson:"dojovalues,omitempty" json:"dojovalues,omitempty"`
	Surveys    []Survey           `bson:"surveys" json:"surveys"`
	TechCoach  *Person            `bson:"techCoach" json:"techCoach"`
	AgileCoach *Person            `bson:"agileCoach" json:"agileCoach"`
	Location   string             `bson:"location" json:"location"`
	StartDate  *time.Time         `bson:"startDate" json:"startDate"`
	EndDate    *time.Time         `bson:"endDate" json:"endDate"`
	Started    *time.Time         `bson:"started" json:"started"`
	Challenges []string           `bson:"challenges" json:"challenges"`
	Completed  *time.Time         `bson:"completed" json:"completed"`
	Notes      []Note             `bson:"notes" json:"notes"`
}

type DojoDB interface {
	Create(dojo *Dojo) error
	List() ([]Dojo, error)
	Retrieve(id string) (*Dojo, error)
	AddRating(id, dojoI string, rating Rating) error
	Update(id string, dojo Dojo) (interface{}, error)
	AllDojos() ([]Dojo, error)
}

type DojoService interface {
	DojoDB
}

type dojoService struct {
	DojoDB
}

type dojoValidator struct {
	DojoDB
}

type dojoMongo struct {
	db         *mongo.Database
	collection string
}

var _ DojoDB = &dojoMongo{}

func (dojoM *dojoMongo) Create(dojo *Dojo) error {
	collection := dojoM.db.Collection(dojoM.collection)
	dojo.Ratings = make([]Rating, 0)
	dojo.DojoValues = make([]DojoValue, 0)
	res, err := collection.InsertOne(context.Background(), dojo)
	if err != nil {
		return err
	}

	dojo.ID = res.InsertedID.(primitive.ObjectID)
	return nil
}

func (dojoM *dojoMongo) List() ([]Dojo, error) {
	dojos := make([]Dojo, 0)

	collection := dojoM.db.Collection(dojoM.collection)
	filter := bson.D{{}}
	cursor, err := collection.Find(context.Background(), filter)
	if err != nil {
		return nil, err
	}

	err = cursor.All(context.Background(), &dojos)
	if err != nil {
		return nil, err
	}
	defer cursor.Close(context.Background())

	return dojos, err
}

func (dojoM *dojoMongo) Retrieve(id string) (*Dojo, error) {
	var dojos []Dojo

	collection := dojoM.db.Collection(dojoM.collection)
	oid, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return nil, err
	}

	filter := bson.M{"_id": oid}
	cursor, err := collection.Find(context.Background(), filter)
	if err != nil {
		return nil, err
	}

	err = cursor.All(context.Background(), &dojos)
	if err != nil {
		return nil, err
	}
	defer cursor.Close(context.Background())

	return &dojos[0], err
}

func (dojoM *dojoMongo) AddRating(id string, dojoI string, rating Rating) error {
	collection := dojoM.db.Collection(dojoM.collection)
	objID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return err
	}

	rating.Date = time.Now()

	updateDojo := bson.M{"$push": bson.M{"ratings": rating}}
	filter := bson.M{"_id": bson.M{"$eq": objID}}
	_, err = collection.UpdateOne(context.Background(), filter, updateDojo)
	if err != nil {
		return err
	}

	return nil
}

func (dojoM *dojoMongo) Update(id string, dojo Dojo) (interface{}, error) {
	collection := dojoM.db.Collection(dojoM.collection)
	objID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return nil, err
	}

	updateDojo := bson.M{"$set": dojo}
	filter := bson.M{"_id": bson.M{"$eq": objID}}
	res, err := collection.UpdateOne(context.Background(), filter, updateDojo)
	if err != nil {
		return nil, err
	}

	return res.UpsertedID, nil
}

func (dojoM *dojoMongo) AllDojos() ([]Dojo, error) {
	dojos := make([]Dojo, 0)
	collection := dojoM.db.Collection(dojoM.collection)
	filter := []bson.M{{"$unwind": "$dojos"}}
	cursor, err := collection.Aggregate(context.Background(), filter)
	if err != nil {
		return nil, err
	}

	err = cursor.All(context.Background(), &dojos)
	if err != nil {
		return nil, err
	}
	defer cursor.Close(context.Background())

	return dojos, err
}

func NewDojoService(db *mongo.Database) DojoService {
	return &dojoService{
		DojoDB: &dojoValidator{
			DojoDB: &dojoMongo{
				db:         db,
				collection: "dojos",
			},
		},
	}
}

type dojoValFn func(*Dojo) error

func runDojoValFns(dojo *Dojo, fns ...dojoValFn) error {
	for _, fn := range fns {
		if err := fn(dojo); err != nil {
			return err
		}
	}
	return nil
}

func (dojoV *dojoValidator) Update(id string, dojo Dojo) (interface{}, error) {
	fmt.Println("checking errors")

	err := runDojoValFns(&dojo, dojoV.allocateArrays)
	if err != nil {
		return nil, err
	}

	return dojoV.DojoDB.Update(id, dojo)
}

func (dojoV *dojoValidator) allocateArrays(dojo *Dojo) error {
	if dojo.Teams == nil {
		dojo.Teams = make([]Team, 0)
	}

	if dojo.Surveys == nil {
		dojo.Surveys = make([]Survey, 0)
	}
	return nil
}

func (dojoV *dojoValidator) Create(dojo *Dojo) error {

	err := runDojoValFns(dojo, dojoV.allocateArrays)
	if err != nil {
		return err
	}

	return dojoV.DojoDB.Create(dojo)
}
