package models

import (
	"context"
	"fmt"
	"log"
	"time"

	"gitlab.com/ace-api/hash"
	"gitlab.com/ace-api/token"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/x/bsonx"
)

const hmacSecretKey = "ace-super-secret-hmac-key"

type Session struct {
	ID           primitive.ObjectID `bson:"_id,omitempty"`
	UserName     string             `bson:"userName`
	RememberHash string             `bson:"rememberHash"`
	CreatedDate  time.Time          `bson:"createdDate"`
}

type SessionDB interface {
	Create(string) (string, error)
	ByToken(tokenString string) (*Session, error)
}

type SessionService interface {
	SessionDB
}

type sessionService struct {
	SessionDB
}

type sessionValidator struct {
	SessionDB
}

type sessionMongo struct {
	db         *mongo.Database
	collection string
	hmac       hash.HMAC
}

func (sm *sessionMongo) Create(name string) (string, error) {
	collection := sm.db.Collection(sm.collection)
	rememberToken, err := token.RememberToken()
	if err != nil {
		return rememberToken, err
	}

	rememberHash, err := sm.hmac.Hash(rememberToken)
	if err != nil {
		return rememberHash, err
	}

	session := Session{
		UserName:     name,
		CreatedDate:  time.Now(),
		RememberHash: rememberHash,
	}
	_, err = collection.InsertOne(context.Background(), &session)
	if err != nil {
		return "", err
	}

	return token.JwtToken(name, rememberToken)
}

func (sm *sessionMongo) ByToken(tokenString string) (*Session, error) {
	var sessions []Session

	rememberString, err := token.GetRemember(tokenString)
	if err != nil {
		return nil, err
	}

	rememberHash, err := sm.hmac.Hash(rememberString)
	if err != nil {
		return nil, err
	}

	collection := sm.db.Collection(sm.collection)
	filter := bson.D{{"rememberHash", rememberHash}}
	cursor, err := collection.Find(context.Background(), filter)
	if err != nil {
		return nil, err
	}

	err = cursor.All(context.Background(), &sessions)
	if err != nil {
		return nil, err
	}

	if len(sessions) == 0 {
		return nil, fmt.Errorf("no session found")
	}
	defer cursor.Close(context.Background())

	return &sessions[0], err
}

func NewSessionService(db *mongo.Database) SessionService {
	collectionName := "sessions"
	var ttl int32 = 30 * 24 * 60 * 60
	hmac := hash.NewHMAC(hmacSecretKey)
	collection := db.Collection(collectionName)

	indexOptions := options.Index().SetExpireAfterSeconds(ttl)
	indexKeys := bsonx.MDoc{
		"createdDate": bsonx.Int32(-1),
	}

	_, err := collection.Indexes().CreateOne(
		context.Background(),
		mongo.IndexModel{
			Options: indexOptions,
			Keys:    indexKeys,
		},
	)
	if err != nil {
		log.Fatal(err)
	}

	return &sessionService{
		SessionDB: &sessionValidator{
			SessionDB: &sessionMongo{
				db:         db,
				collection: collectionName,
				hmac:       hmac,
			},
		},
	}
}
