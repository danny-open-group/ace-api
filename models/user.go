package models

import (
	"context"
	"errors"
	"fmt"
	"log"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/x/bsonx"
	"golang.org/x/crypto/bcrypt"
)

var (
	userPwPepper       = "ace-secret-pepper"
	ErrInvalidPassword = errors.New("models: incorrect password provided")
)

type User struct {
	ID           primitive.ObjectID `bson:"_id,omitempty" json:"-"`
	Username     string             `bson:"username" json:"username"`
	Password     string             `bson:"-" json:"password,omitempty"`
	PasswordHash string             `bson:"passwordHash" json:"-"`
}

type UserDB interface {
	Create(user *User) error
	Delete(user *User) (int, error)
	ByName(name string) (*User, error)
	Authenticate(name, password string) (*User, error)
}

type UserService interface {
	UserDB
}

type userService struct {
	UserDB
}

type userValidator struct {
	UserDB
}

type userMongo struct {
	db         *mongo.Database
	collection string
}

func (um *userMongo) Create(user *User) error {
	collection := um.db.Collection(um.collection)
	pwBytes := []byte(user.Password + userPwPepper)
	hashedBytes, err := bcrypt.GenerateFromPassword(pwBytes, bcrypt.DefaultCost)
	if err != nil {
		return err
	}

	user.PasswordHash = string(hashedBytes)
	user.Password = ""
	log.Printf("\n%v\n", user)
	id, err := collection.InsertOne(context.Background(), user)
	if err != nil {
		return err
	}

	user.ID = id.InsertedID.(primitive.ObjectID)
	return nil
}

func (um *userMongo) Delete(user *User) (int, error) {
	collection := um.db.Collection(um.collection)
	filter := bson.D{{"username", user.Username}}

	deleteResult, err := collection.DeleteMany(context.Background(), filter)
	if err != nil {
		return 0, err
	}

	return int(deleteResult.DeletedCount), nil
}

func (um *userMongo) Authenticate(name, password string) (*User, error) {
	foundUser, err := um.ByName(name)
	if err != nil {
		return nil, err
	}

	err = bcrypt.CompareHashAndPassword(
		[]byte(foundUser.PasswordHash),
		[]byte(password+userPwPepper))
	switch err {
	case nil:
		foundUser.Password = ""
		return foundUser, nil
	case bcrypt.ErrMismatchedHashAndPassword:
		return nil, ErrInvalidPassword
	default:
		return nil, err
	}
}

func (um *userMongo) ByName(name string) (*User, error) {
	var users []User

	collection := um.db.Collection(um.collection)
	filter := bson.D{{"username", name}}
	cursor, err := collection.Find(context.Background(), filter)
	if err != nil {
		return nil, err
	}

	err = cursor.All(context.Background(), &users)
	if err != nil {
		return nil, err
	}
	if len(users) == 0 {
		return nil, fmt.Errorf("username %v not found", name)
	}
	defer cursor.Close(context.Background())
	log.Printf("Username Found: %v\n", &users)
	return &users[0], nil
}

func NewUserService(db *mongo.Database) UserService {
	collectionName := "users"
	collection := db.Collection(collectionName)
	indexOptions := options.Index().SetUnique(true)
	indexKeys := bsonx.MDoc{
		"username": bsonx.Int32(-1),
	}

	_, err := collection.Indexes().CreateOne(
		context.Background(),
		mongo.IndexModel{
			Options: indexOptions,
			Keys:    indexKeys,
		},
	)
	if err != nil {
		log.Fatal(err)
	}

	us := &userService{
		UserDB: &userValidator{
			UserDB: &userMongo{
				db:         db,
				collection: collectionName,
			},
		},
	}

	count, err := us.Delete(&User{
		Username: "AceAdmin",
	})
	if err != nil {
		log.Printf("Error deleting AceAdmin user: %v\n", err)
	}
	if count > 0 {
		log.Printf("Removed AceAdmin password")
	}

	return us
}
