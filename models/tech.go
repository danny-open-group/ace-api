package models

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Tech struct {
	ID          primitive.ObjectID `bson:"_id" json:"id"`
	Name        string             `bson:"name" json:"name"`
	Description string             `bson:"description" json:"description"`
	Version     string             `bson:"version" json:"version"`
	Badge       string             `bson:"badge" json:"badge"`
	HREF        string             `bson:"href" json:"href"`
}
