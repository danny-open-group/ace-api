package models

import (
	"context"
	"errors"
	"fmt"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

var (
	ErrNameRequired = errors.New("models: Team requires a name")
)

type Team struct {
	ID         primitive.ObjectID `bson:"_id,omitempty" json:"id,omitempty"`
	Rank       float64            `bson:"rank" json:"rank"`
	Name       string             `bson:"name" json:"name"`
	Location   string             `bson:"location" json:"location"`
	TechStack  string             `bson:"techStack" json:"techStack"`
	Leader     *Person            `bson:"leader" json:"leader,omitempty"`
	Completed  *time.Time         `bson:"completed" json:"completed,omitempty"`
	DojoRating float64            `bson:"dojoRating" json:"dojoRating"`
	Members    []Person           `bson:"members" json:"members"`
	Challenges []string           `bson:"challenges" json:"challenges"`
	Dojos      []Dojo             `bson:"dojos" json:"dojos"`
	Ratings    []Rating           `bson:"ratings" json:"ratings"`
	Assessments []Assessment      `bson:"assessments,omitempty"  json:"assessments,omitempty"`

}

type TeamDB interface {
	Create(team *Team) error
	List() ([]Team, error)
	Retrieve(id string) (*Team, error)
	AddRating(id, dojoI string, rating Rating) error
	Update(id string, team Team) (interface{}, error)
	AllDojos() ([]Dojo, error)
}

type TeamService interface {
	TeamDB
}

type teamService struct {
	TeamDB
}

type teamValidator struct {
	TeamDB
}

type teamMongo struct {
	db         *mongo.Database
	collection string
}

var _ TeamDB = &teamMongo{}

func (tm *teamMongo) Create(team *Team) error {
	collection := tm.db.Collection(tm.collection)
	res, err := collection.InsertOne(context.Background(), team)
	if err != nil {
		return err
	}

	team.ID = res.InsertedID.(primitive.ObjectID)
	return nil
}

func (tm *teamMongo) List() ([]Team, error) {
	teams := make([]Team, 0)

	collection := tm.db.Collection(tm.collection)
	filter := bson.D{{}}
	cursor, err := collection.Find(context.Background(), filter)
	if err != nil {
		return nil, err
	}

	err = cursor.All(context.Background(), &teams)
	if err != nil {
		return nil, err
	}
	defer cursor.Close(context.Background())

	return teams, err
}

func (tm *teamMongo) Retrieve(id string) (*Team, error) {
	var teams []Team

	collection := tm.db.Collection(tm.collection)
	oid, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return nil, err
	}

	filter := bson.M{"_id": oid}
	cursor, err := collection.Find(context.Background(), filter)
	if err != nil {
		return nil, err
	}

	err = cursor.All(context.Background(), &teams)
	if err != nil {
		return nil, err
	}
	defer cursor.Close(context.Background())

	return &teams[0], err
}

func (tm *teamMongo) AddRating(id string, dojoI string, rating Rating) error {
	collection := tm.db.Collection(tm.collection)
	objID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return err
	}

	rating.Date = time.Now()

	updateTeam := bson.M{"$push": bson.M{"dojos." + dojoI + ".ratings": rating}}
	filter := bson.M{"_id": bson.M{"$eq": objID}}
	_, err = collection.UpdateOne(context.Background(), filter, updateTeam)
	if err != nil {
		return err
	}

	return nil
}

func (tm *teamMongo) Update(id string, team Team) (interface{}, error) {
	collection := tm.db.Collection(tm.collection)
	objID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return nil, err
	}

	updateTeam := bson.M{"$set": team}
	filter := bson.M{"_id": bson.M{"$eq": objID}}
	res, err := collection.UpdateOne(context.Background(), filter, updateTeam)
	if err != nil {
		return nil, err
	}

	return res.UpsertedID, nil
}

func (tm *teamMongo) AllDojos() ([]Dojo, error) {
	dojos := make([]Dojo, 0)
	collection := tm.db.Collection(tm.collection)
	filter := []bson.M{{"$unwind": "$dojos"}}
	cursor, err := collection.Aggregate(context.Background(), filter)
	if err != nil {
		return nil, err
	}

	err = cursor.All(context.Background(), &dojos)
	if err != nil {
		return nil, err
	}
	defer cursor.Close(context.Background())

	return dojos, err
}

func NewTeamService(db *mongo.Database) TeamService {
	return &teamService{
		TeamDB: &teamValidator{
			TeamDB: &teamMongo{
				db:         db,
				collection: "teams",
			},
		},
	}
}

type teamValFn func(*Team) error

func runTeamValFns(team *Team, fns ...teamValFn) error {
	for _, fn := range fns {
		if err := fn(team); err != nil {
			return err
		}
	}
	return nil
}

func (tv *teamValidator) nameRequired(t *Team) error {
	if t.Name == "" {
		return fmt.Errorf("name is required")
	}
	return nil
}

func allDojosCompleted(dojos []Dojo) bool {
	for _, dojo := range dojos {
		if dojo.EndDate.IsZero() {
			return false
		}
	}

	return true
}

func (tv *teamValidator) checkCompletionCriteria(t *Team) error {
	if t.Completed != nil && !t.Completed.IsZero() {
		if len(t.Challenges) < 1 {
			return ErrChallengesMissing
		}

		if !allDojosCompleted(t.Dojos) {
			return ErrDojosOpen
		}
	}

	return nil
}

func (tv *teamValidator) Update(id string, team Team) (interface{}, error) {
	fmt.Println("checking errors")
	err := runTeamValFns(&team, tv.checkCompletionCriteria)
	if err != nil {
		fmt.Println("got an error:", err.Error())
		return nil, err
	}

	return tv.TeamDB.Update(id, team)
}

func (tv *teamValidator) Create(team *Team) error {
	err := runTeamValFns(team, tv.nameRequired)
	if err != nil {
		return err
	}

	return tv.TeamDB.Create(team)
}
