package context

import (
	"context"

	"gitlab.com/ace-api/models"
)

type privateKey string

const (
	sessionKey privateKey = "session"
)

func WithSession(ctx context.Context, session *models.Session) context.Context {
	return context.WithValue(ctx, sessionKey, session)
}

func Session(ctx context.Context) *models.Session {
	if temp := ctx.Value(sessionKey); temp != nil {
		if session, ok := temp.(*models.Session); ok {
			return session
		}
	}
	return nil
}
